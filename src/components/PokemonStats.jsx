// PokemonStats
import React from 'react';

const PokemonStats = ({ stats }) => (
  <>
    {
      stats.map(
        (item) => (
          <div className="stats-badge" key={item.stat.name}>
            {item.stat.name}
            <div className="stats-number">
              {item.base_stat}
            </div>
          </div>
        ),
      )
    }
  </>
);

export default PokemonStats;
