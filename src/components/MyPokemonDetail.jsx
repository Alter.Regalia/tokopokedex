// MyPokemonDetail
import React, { useState } from 'react';
import { connect } from 'react-redux';

import moment from 'moment';
import NicknameInput from './NicknameInput';

import { pokemonByIdSelector } from '../store/MyPokemon.selector';
import { myPokemonUpdate } from '../store/MyPokemon';

const MyPokemonDetail = ({ pokemon, pokemonUpdate, afterUpdate }) => {
  const [nickname, setNickname] = useState('');

  const handleNickname = (event) => {
    event.preventDefault();
    setNickname(event.target.value);
  };

  const resetNickname = () => {
    setNickname('');
  };

  const updateMyPokemon = () => {
    const pokemonData = {
      ...pokemon,
      nickname,
    };

    pokemonUpdate(pokemonData);
    resetNickname();
    afterUpdate();
  };

  return (
    <>
      {
        pokemon
        && (
          <>
            <p>{`Give ${pokemon.name} nickname`}</p>
            {}
            <span>{`Caught at ${moment(pokemon.catchAt).format('MMMM Do YYYY, h:mm:ss a')}`}</span>
          </>
        )
      }
      <br />
      <div className="pokemon-nickname">
        <NicknameInput handleNickname={handleNickname} />
        <div className="nickname-close">!</div>
      </div>
      <button className="primary-button" type="button" onClick={() => updateMyPokemon()}>Confirm</button>
    </>
  );
};

const mapStateToProps = (state) => ({
  pokemon: pokemonByIdSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  pokemonUpdate: (pokemonData) => {
    dispatch(myPokemonUpdate(pokemonData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MyPokemonDetail);
