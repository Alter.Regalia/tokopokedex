// NavigationBar
import React from 'react';
import { Link } from 'react-router-dom';

const NavigationBar = () => (
  <nav className="navigation-bar">
    <Link to="/" className="navigation-home">
      <p>Pokedex</p>
    </Link>
    <Link to="/MyPokemon" className="navigation-home">
      <p>My Pokemon</p>
    </Link>
  </nav>
);
export default NavigationBar;
