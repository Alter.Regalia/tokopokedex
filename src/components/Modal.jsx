// Modal

/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { useCallback, useEffect } from 'react';

const Modal = ({
  children, show, close,
}) => {
  const escFunction = useCallback((event) => {
    if (event.keyCode === 27) {
      close();
    }
  }, []);

  useEffect(() => {
    document.addEventListener('keydown', escFunction, false);

    return () => {
      document.removeEventListener('keydown', escFunction, false);
    };
  }, []);

  return (
    <div
      className={show ? 'modal modal-active' : 'modal'}
    >
      <div className="modal-container">
        {children}
      </div>
      <div className="modal-button" onClick={close}>Close</div>
    </div>
  );
};

export default Modal;
