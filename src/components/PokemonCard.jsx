// PokemonCard
import React from 'react';

import PokemonStats from './PokemonStats';
import PokemonTypes from './PokemonTypes';

const PokemonCard = ({ pokemon }) => (
  <>
    <div className="pokemon-card">
      <div className="pokemon-title">
        <p>{`#${pokemon.id} ${pokemon.name}`}</p>
      </div>
      <div className="pokemon-image">
        <img src={pokemon.sprites.front_default} alt={pokemon.name} />
        <img src={pokemon.sprites.back_default} alt={pokemon.name} />
      </div>
      <div className="pokemon-types">
        <PokemonTypes types={pokemon.types} />
      </div>
      <div className="pokemon-size">
        <p>{`Weight: ${pokemon.weight} / Height: ${pokemon.height}`}</p>
      </div>
      <div className="pokemon-description">
        {pokemon.classification && <p>{pokemon.classification.replace('\n', '<br />')}</p>}
        {pokemon.flavorText && <p>{pokemon.flavorText}</p>}
      </div>
      <div className="pokemon-stats">
        <PokemonStats stats={pokemon.stats} />
      </div>
    </div>
  </>
);

export default PokemonCard;
