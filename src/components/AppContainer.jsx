// App Container
import React, { useState } from 'react';
import InfiniteScroll from './InfiniteScroll';

const AppContainer = ({ children }) => {
  const [fetchMore, setFetchMore] = useState(false);

  const scrollHandler = ({ currentTarget }) => {
    if (
      currentTarget.scrollTop + currentTarget.clientHeight
      >= currentTarget.scrollHeight - 50
    ) {
      setFetchMore(true);
    } else {
      setFetchMore(false);
    }
  };

  return (
    <div className="app-container" onScroll={scrollHandler}>
      <InfiniteScroll.Provider value={fetchMore}>
        {children}
      </InfiniteScroll.Provider>
    </div>
  );
};

export default AppContainer;
