// MyPokemonCard
import React from 'react';
import moment from 'moment';
import { connect } from 'react-redux';

import { myPokemonRelease } from '../store/MyPokemon';

const MyPokemonCard = ({ pokemon, selectPokemon, releasePokemon }) => (
  <>
    <div className="mypokemon-card">
      <div className="mypokemon-header">
        <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${pokemon.id}.png`} alt={pokemon.name} />
        <p className="mypokemon-title">{pokemon.name}</p>
        <span>{`Caught at ${moment(pokemon.catchAt).format('MMMM Do YYYY, h:mm:ss a')}`}</span>

      </div>
      <div className="mypokemon-content">
        {
          pokemon.nickname
            ? <span className="mypokemon-nickname">{pokemon.nickname}</span>
            : <button className="nickname-button" type="button" onClick={selectPokemon}>Add Nickname</button>
        }
        <button className="secondary-button" type="button" onClick={() => releasePokemon(pokemon.pokemonId)}>Release</button>
      </div>
    </div>
  </>
);

const mapDispatchToProps = (dispatch) => ({
  releasePokemon: (pokemonId) => {
    dispatch(myPokemonRelease(pokemonId));
  },
});

export default connect(null, mapDispatchToProps)(MyPokemonCard);
