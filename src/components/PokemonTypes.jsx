// PokemonTypes
import React from 'react';

const PokemonTypes = ({ types }) => (
  <>
    {types.map((item) => (
      <div className="type-badge" key={item.slot}>
        {item.type.name}
      </div>
    ))}
  </>
);

export default PokemonTypes;
