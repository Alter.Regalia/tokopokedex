// PokedexDetail
import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';

import PokemonCard from './PokemonCard';
import NicknameInput from './NicknameInput';

import { myPokemonAdd } from '../store/MyPokemon';
import { pokemonByNameSelector } from '../store/Pokemon.selector';

const PokedexDetail = ({ pokemon, pokemonAdd }) => {
  const [catching, setCatching] = useState(false);
  const [success, setSuccess] = useState(false);
  const [addNickname, setAddNickname] = useState(false);
  const [nickname, setNickname] = useState('');

  const stateReset = () => {
    setNickname('');
    setSuccess(false);
    setCatching(false);
    setAddNickname(false);
  };

  const randomizer = () => Math.random() >= 0.5;

  const catchPokemon = () => {
    setCatching(true);

    setTimeout(() => {
      setCatching(false);
      setSuccess(randomizer());
    }, 3000);
  };

  const handleNickname = (event) => {
    event.preventDefault();
    setNickname(event.target.value);
  };

  const addToMyPokemon = () => {
    const pokemonData = {
      ...pokemon,
      nickname,
    };

    pokemonAdd(pokemonData);
    stateReset();
  };


  useEffect(() => {
    stateReset();
  }, [pokemon]);

  if (!pokemon) {
    return '...loading';
  }

  return (
    <>
      <div className="pokemon-detail">
        {!success ? (
          <>
            <PokemonCard pokemon={pokemon} />
            <button className="primary-button" type="button" onClick={() => catchPokemon()}>
              {catching ? 'Catching...' : 'Catch'}
            </button>
          </>
        ) : (
          <>
            {`Wild ${pokemon.name} is caught!`}
            <div className="pokemon-image">
              <img src={pokemon.sprites.front_default} alt={pokemon.name} />
              <img src={pokemon.sprites.back_default} alt={pokemon.name} />
            </div>
            {addNickname
              ? (
                <div className="pokemon-nickname">
                  <NicknameInput handleNickname={handleNickname} />
                  <button type="button" className="nickname-close" onClick={() => setAddNickname(!addNickname)}>x</button>
                </div>
              )
              : (<button className="nickname-button" type="button" onClick={() => setAddNickname(!addNickname)}>Add Nickname</button>)}
            <button className="primary-button" type="button" onClick={() => addToMyPokemon()}>Confirm</button>
            <button className="secondary-button" type="button" onClick={() => stateReset()}>Release</button>
          </>
        )}
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  pokemon: pokemonByNameSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  pokemonAdd: (pokemonData) => {
    dispatch(myPokemonAdd(pokemonData));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(PokedexDetail);
