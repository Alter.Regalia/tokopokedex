// InfiniteScroll - Context
import { createContext } from 'react';

const InfiniteScroll = createContext(null);

export default InfiniteScroll;
