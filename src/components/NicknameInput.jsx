// NicknameInput
import React from 'react';

const NicknameInput = ({ handleNickname }) => <input className="nickname-input" type="text" placeholder="Nickname" onChange={handleNickname} />;

export default NicknameInput;
