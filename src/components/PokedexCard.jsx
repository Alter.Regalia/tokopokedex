// PokedexCard

/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React from 'react';

const PokedexCard = ({ pokemon, onCardClick, index }) => (
  <>
    <div onClick={onCardClick} className="pokedex-card">
      <img src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${index}.png`} alt={pokemon.name} />
      <p className="pokedex-card-title">{pokemon.name}</p>
    </div>
  </>
);
export default PokedexCard;
