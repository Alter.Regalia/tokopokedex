// App
import React from 'react';
import {
  BrowserRouter as Router, Switch, Route,
} from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';

import Pokedex from './screens/Pokedex';
import MyPokemon from './screens/MyPokemon';

import NavigationBar from './components/NavigationBar';
import AppContainer from './components/AppContainer';

const App = () => (
  <Provider store={store}>
    <Router>
      <AppContainer>
        <Switch>
          <Route path="/MyPokemon">
            <MyPokemon />
          </Route>
          <Route path="/">
            <Pokedex />
          </Route>
        </Switch>
      </AppContainer>
      <NavigationBar />
    </Router>
  </Provider>
);

export default App;
