// Pokemon Store Selector
import { createSelector } from 'reselect';
import { selectedNameSelector } from './Utils.selector';

export const pokemonStoreSelector = (state) => state.PokemonStore;

export const pokemonsSelector = createSelector(
  [pokemonStoreSelector],
  (pokemonStore) => pokemonStore.pokemons,
);

export const pokemonByNameSelector = createSelector(
  [pokemonsSelector, selectedNameSelector],
  (pokemons, selectedName) => pokemons.filter((pokemon) => pokemon.name === selectedName)[0],
);
