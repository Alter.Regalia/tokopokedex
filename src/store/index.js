// Stores
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';

import ReduxThunk from 'redux-thunk';
import reducer from './reducer';

const middleware = [ReduxThunk];

const store = createStore(reducer, undefined, composeWithDevTools(applyMiddleware(...middleware)));

export default store;
