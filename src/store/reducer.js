// Reducer
import { combineReducers } from 'redux';

import { pokedexReducer } from './Pokedex';
import { pokemonReducer } from './Pokemon';
import { myPokemonReducer } from './MyPokemon';
import { utilsReducer } from './Utils';

const reducer = combineReducers({
  PokedexStore: pokedexReducer,
  PokemonStore: pokemonReducer,
  MyPokemonStore: myPokemonReducer,
  UtilsStore: utilsReducer,
});

export default reducer;
