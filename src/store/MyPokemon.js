// MyPokemon Store
import moment from 'moment';
import uuid from 'uuid/v4';

// Actions Type
const actions = {
  MyPokemonAdd: '[My Pokemon] Add',
  MyPokemonRelease: '[My Pokemon] Release',
  MyPokemonUpdate: '[My Pokemon] Update',
  MyPokemonReset: '[My Pokemon] Reset',
};

// Actions Creator
export const myPokemonAdd = (pokemonData) => ({
  type: actions.MyPokemonAdd,
  payload: {
    pokemonId: uuid(),
    catchAt: moment(),
    ...pokemonData,
  },
});

export const myPokemonRelease = (pokemonId) => ({
  type: actions.MyPokemonRelease,
  payload: {
    pokemonId,
  },
});

export const myPokemonUpdate = (pokemonData) => ({
  type: actions.MyPokemonUpdate,
  payload: {
    ...pokemonData,
  },
});

export const myPokemonReset = () => ({
  type: actions.MyPokemonReset,
});

// Initial State
const InitialState = {
  pokemons: [],
};

// MyPokemon Reducer
export const myPokemonReducer = (state = InitialState, action) => {
  switch (action.type) {
    case actions.MyPokemonAdd:
      return {
        ...state,
        pokemons: [
          ...state.pokemons,
          action.payload,
        ],
      };

    case actions.MyPokemonRelease:
      return {
        ...state,
        pokemons: state.pokemons.filter(
          (pokemon) => pokemon.pokemonId !== action.payload.pokemonId,
        ),
      };

    case actions.MyPokemonUpdate:
      return {
        ...state,
        pokemons: state.pokemons.map(
          (pokemon) => (
            pokemon.pokemonId === action.payload.pokemonId
              ? { ...action.payload }
              : pokemon),
        ),
      };

    case actions.MyPokemonReset:
      return {
        ...InitialState,
      };

    default:
      return state;
  }
};
