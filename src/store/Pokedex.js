// Pokedex Store
import axios from 'axios';

// Actions Types
const actions = {
  PokedexRequest: '[POKEDEX] Fetch Request',
  PokedexSuccess: '[POKEDEX] Fetch Success',
  PokedexFailed: '[POKEDEX] Fetch Failed',
};

// Actions Creator
const pokedexRequest = () => ({
  type: actions.PokedexRequest,
});

const pokedexSuccess = (dataSuccess) => ({
  type: actions.PokedexSuccess,
  payload: dataSuccess,
});

const pokedexFailed = (dataFailed) => ({
  type: actions.PokedexFailed,
  payload: dataFailed,
});

// Initial State
const initialState = {
  pokemons: [],
  pokemonsIsFetching: false,
  pokemonsError: {},
  count: 0,
};

// Pokedex Reducer
export const pokedexReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.PokedexRequest:
      return {
        ...state,
        pokemonsIsFetching: true,
      };

    case actions.PokedexSuccess:
      return {
        ...state,
        pokemons: [...state.pokemons, ...action.payload.data.results],
        pokemonsIsFetching: false,
        count: action.payload.data.count,
      };

    default:
      return state;
  }
};

// Action Dispatcher - thunk
export const fetchPokedex = (limit, offset) => (dispatch) => {
  dispatch(pokedexRequest());

  axios.get(`https://pokeapi.co/api/v2/pokemon/?limit=${limit}&offset=${offset}`)
    .then((response) => dispatch(pokedexSuccess(response)))
    .catch((error) => dispatch(pokedexFailed(error)));
};
