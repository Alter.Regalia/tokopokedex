// Utils Store
const actions = {
  UtilsSelectPokedex: '[Utils] Select Pokedex',
  UtilsSelectMyPokemon: '[Utils] Select MyPokemon',
  UtilsReset: '[Utils] Reset',
};

// Actions Creator
export const utilsPokedexSelect = (name) => ({
  type: actions.UtilsSelectPokedex,
  payload: {
    name,
  },
});

export const utilsMyPokemonSelect = (id) => ({
  type: actions.UtilsSelectMyPokemon,
  payload: {
    id,
  },
});

export const utilsReset = () => ({
  type: actions.UtilsReset,
});

// Initial State
const initialState = {
  selectedName: '',
  selectedMyPokemon: '',
};

// Utils Reducer
export const utilsReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.UtilsSelectPokedex:
      return {
        ...state,
        selectedName: action.payload.name,
      };

    case actions.UtilsSelectMyPokemon:
      return {
        ...state,
        selectedMyPokemon: action.payload.id,
      };

    case actions.UtilsReset:
      return {
        ...initialState,
      };

    default:
      return state;
  }
};
