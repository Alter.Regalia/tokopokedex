// Utils Store Selector
import { createSelector } from 'reselect';

export const UtilsStoreSelector = (state) => state.UtilsStore;

export const selectedNameSelector = createSelector(
  [UtilsStoreSelector],
  (utilsStore) => utilsStore.selectedName,
);

export const selectedMyPokemonSelector = createSelector(
  [UtilsStoreSelector],
  (utilsStore) => utilsStore.selectedMyPokemon,
);
