// MyPokemon Store Selector
import { createSelector } from 'reselect';
import { selectedMyPokemonSelector } from './Utils.selector';

export const myPokemonStoreSelector = (state) => state.MyPokemonStore;

export const pokemonsSelector = createSelector(
  [myPokemonStoreSelector],
  (myPokemonStore) => myPokemonStore.pokemons,
);

export const pokemonByIdSelector = createSelector(
  [pokemonsSelector, selectedMyPokemonSelector],
  (pokemons, selectedMyPokemon) => pokemons.filter(
    (pokemon) => pokemon.pokemonId === selectedMyPokemon,
  )[0],
);

export const pokemonKindSelector = createSelector(
  [pokemonsSelector],
  (pokemons) => {
    const kind = {};

    pokemons.forEach((pokemon) => {
      kind[pokemon.name] = (kind[pokemon.name] || 0) + 1;
    });

    return Object.keys(kind).length;
  },
);
