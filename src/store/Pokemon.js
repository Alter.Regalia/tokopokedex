// Pokemon Store
import axios from 'axios';

// Action Types
const actions = {
  PokemonRequest: '[POKEMON] Fetch Request',
  PokemonSuccess: '[POKEMON] Fetch Success',
  PokemonFailed: '[POKEMON] Fetch Failed',

  PokemonAdd: '[POKEMON] Add',
  PokemonUpdate: '[POKEMON] Update',
  PokemonReset: '[POKEMON] Reset',
};

// Actions Creator
export const pokemonRequest = () => ({
  type: actions.PokemonRequest,
});

export const pokemonSuccess = () => ({
  type: actions.PokemonSuccess,
});

export const pokemonFailed = (dataFailed) => ({
  type: actions.PokemonFailed,
  payload: dataFailed,
});

export const pokemonAdd = (pokemon) => ({
  type: actions.PokemonAdd,
  payload: {
    data: {
      id: pokemon.id,
      name: pokemon.name,
      height: pokemon.height,
      weight: pokemon.weight,
      sprites: pokemon.sprites,
      stats: pokemon.stats,
      types: pokemon.types,
    },
  },
});

export const pokemonUpdate = (species) => ({
  type: actions.PokemonUpdate,
  payload: {
    data: {
      id: species.id,
      captureRate: species.capture_rate,
      baseHappiness: species.base_happiness,
      flavorText: species.flavor_text_entries.filter((flavor) => flavor.language.name === 'en' && flavor.version.name === 'y')[0].flavor_text,
      classification: species.genera.filter((genera) => genera.language.name === 'en')[0].genus,
    },
  },
});

// Initial State
const initialState = {
  pokemons: [],
  pokemonIsFetching: false,
  pokemonError: {},
};

// Pokemon Reducer
export const pokemonReducer = (state = initialState, action) => {
  switch (action.type) {
    case actions.PokemonRequest:
      return {
        ...state,
        pokemonIsFetching: true,
      };

    case actions.PokemonSuccess:
      return {
        ...state,
        pokemonIsFetching: false,
      };

    case actions.PokemonFailed:
      return {
        ...state,
        pokemonIsFetching: false,
      };

    case actions.PokemonAdd:
      return {
        ...state,
        pokemons: [
          ...state.pokemons,
          action.payload.data,
        ],
      };


    case actions.PokemonUpdate:
      return {
        ...state,
        pokemons: state.pokemons.map(
          (pokemon) => (
            pokemon.id === action.payload.data.id
              ? { ...pokemon, ...action.payload.data }
              : pokemon),
        ),
      };

    default:
      return state;
  }
};

// Action Dispatcher - thunk
export const fetchPokemon = (pokemonName) => (dispatch, state) => {
  const statePokemons = state().PokemonStore.pokemons;

  const pokemonUrl = `https://pokeapi.co/api/v2/pokemon/${pokemonName}/`;
  const pokemonSpeciesUrl = (pokemeonSpeciesName) => `https://pokeapi.co/api/v2/pokemon-species/${pokemeonSpeciesName}`;

  if (statePokemons.filter((statePokemon) => statePokemon.name === pokemonName).length === 0) {
    dispatch(pokemonRequest());

    axios.get(pokemonUrl)
      .then((pokemon) => {
        dispatch(pokemonAdd(pokemon.data));
        return axios.get(pokemonSpeciesUrl(pokemonName));
      })
      .then((species) => {
        dispatch(pokemonUpdate(species.data));
        dispatch(pokemonSuccess());
      })

      .catch((error) => dispatch(pokemonFailed(error)));
  }
};
