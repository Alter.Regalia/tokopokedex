// Pokedex Store Selector
import { createSelector } from 'reselect';

export const pokedexStoreSelector = (state) => state.PokedexStore;

export const pokedexPokemonsSelector = createSelector(
  [pokedexStoreSelector],
  (pokedexStore) => pokedexStore.pokemons,
);

export const pokedexCountSelector = createSelector(
  [pokedexStoreSelector],
  (pokedexStore) => pokedexStore.count,
);
