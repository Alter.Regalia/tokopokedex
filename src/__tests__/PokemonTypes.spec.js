import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import PokemonTypes from '../components/PokemonTypes';

describe('PokemonTypes', () => {
  const typesArr = [{
    slot: 1,
    type: {
      name: 'grass',
    },
  }];

  const PokemonTypes$ = () => (
    <PokemonTypes types={typesArr} />
  );

  it('Match Snapshot!', () => {
    const component = renderer.create(PokemonTypes$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should render list of types', () => {
    const wrapper = mount(<PokemonTypes types={typesArr} />);
    expect(wrapper.children()).toHaveLength(1);
  });
});
