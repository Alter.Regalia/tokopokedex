import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import PokemonStats from '../components/PokemonStats';

describe('PokemonStats', () => {
  const statsArr = [
    {
      base_stat: 60,
      stat: {
        name: 'speed',
      },
    },
    {
      base_stat: 80,
      stat: {
        name: 'special-defense',
      },
    },
  ];

  const PokemonStats$ = () => (<PokemonStats stats={statsArr} />);

  it('Match Snapshot!', () => {
    const component = renderer.create(PokemonStats$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should render list of stats', () => {
    const wrapper = mount(<PokemonStats stats={statsArr} />);
    expect(wrapper.children()).toHaveLength(2);
  });
});
