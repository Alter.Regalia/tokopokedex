// Pokemon Store Selector Test
import { pokemonStoreSelector, pokemonsSelector, pokemonByNameSelector } from '../store/Pokemon.selector';

const globalStore = {
  PokedexStore: {},
  MyPokemonStore: {},
  PokemonStore: {
    pokemons: [
      {
        id: 1,
        name: 'bulbasaur',
        height: 30,
        weight: 60,
        sprites: {},
        stats: [],
        types: [],
      },
    ],
  },
  UtilsStore: {
    selectedName: 'bulbasaur',
  },
};

describe('PokemonStore selector', () => {
  it('Should return PokemonStore', () => {
    const result = pokemonStoreSelector(globalStore);
    const expected = {
      pokemons: [
        {
          id: 1,
          name: 'bulbasaur',
          height: 30,
          weight: 60,
          sprites: {},
          stats: [],
          types: [],
        },
      ],
    };

    expect(result).toStrictEqual(expected);
  });

  it('Should return Pokemons', () => {
    const result = pokemonsSelector(globalStore);
    const expected = [
      {
        id: 1,
        name: 'bulbasaur',
        height: 30,
        weight: 60,
        sprites: {},
        stats: [],
        types: [],
      },
    ];

    expect(result).toStrictEqual(expected);
  });

  it('Should return PokemonByName', () => {
    const result = pokemonByNameSelector(globalStore);
    const expected = {
      id: 1,
      name: 'bulbasaur',
      height: 30,
      weight: 60,
      sprites: {},
      stats: [],
      types: [],
    };

    expect(result).toStrictEqual(expected);
  });
});
