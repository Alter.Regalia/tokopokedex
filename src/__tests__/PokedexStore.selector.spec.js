// Pokedex Store Selector Test
import { pokedexStoreSelector, pokedexPokemonsSelector, pokedexCountSelector } from '../store/Pokedex.selector';

const globalStore = {
  PokedexStore: {
    pokemons: [
      { name: 'bulbasaur' },
      { name: 'ivysaur' },
    ],
    count: 2,
  },
  MyPokemonStore: {},
  UtilsStore: {
    selectedName: 'bulbasaur',
    selectedMyPokemon: '',
  },
};

describe('PokedexStore selector', () => {
  it('Should return PokedexStore', () => {
    const result = pokedexStoreSelector(globalStore);
    const expected = {
      pokemons: [
        { name: 'bulbasaur' },
        { name: 'ivysaur' },
      ],
      count: 2,
    };

    expect(result).toStrictEqual(expected);
  });

  it('Should return Pokemons', () => {
    const result = pokedexPokemonsSelector(globalStore);
    const expected = [
      { name: 'bulbasaur' },
      { name: 'ivysaur' },
    ];

    expect(result).toStrictEqual(expected);
  });

  it('Should return Count', () => {
    const result = pokedexCountSelector(globalStore);
    const expected = 2;

    expect(result).toStrictEqual(expected);
  });
});
