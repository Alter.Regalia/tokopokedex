// MyPokemonDetail Test
import React from 'react';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import MyPokemonDetail from '../components/MyPokemonDetail';

// Mock Store
const mockStore = configureStore([thunk]);
const initialState = {
  MyPokemonStore: {
    pokemons: [{
      id: 1,
      name: 'bulbasaur',
      nickname: null,
      catchAt: 1576133297202,
      pokemonId: 'pokemon-01',
    }],
  },
  UtilsStore: {
    selectedMyPokemon: 'pokemon-01',
  },
};
const store = mockStore(initialState);

describe('MyPokemonDetail', () => {
  const MyPokemonDetail$ = () => (
    <Provider store={store}>
      <MyPokemonDetail />
    </Provider>
  );

  it('Match Snapshot!', () => {
    const component = renderer.create(MyPokemonDetail$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
