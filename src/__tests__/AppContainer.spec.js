import React from 'react';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

import AppContainer from '../components/AppContainer';
import InfiniteScroll from '../components/InfiniteScroll';

describe('AppContainer', () => {
  it('Match Snapshot!', () => {
    const component = renderer.create(<AppContainer />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should render children component', () => {
    const props = {
      children: <p>childrens</p>,
    };

    const wrapper = shallow(
      <InfiniteScroll.Provider>
        <AppContainer {...props} />
      </InfiniteScroll.Provider>,
    );
    expect(wrapper.prop('children')).toStrictEqual(<p>childrens</p>);
  });
});
