// MyPokemonCard Test
import React from 'react';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';
import { mount } from 'enzyme';

import MyPokemonCard from '../components/MyPokemonCard';

// Mock Store
const mockStore = configureStore([thunk]);
const initialState = {
  MyPokemonStore: {},
};
const store = mockStore(initialState);

describe('MyPokemonCard', () => {
  const pokemonObj = {
    id: 1,
    name: 'bulbasaur',
    nickname: null,
    catchAt: 1576133297202,
    pokemonId: 'pokemon-01',
  };

  const MyPokemonCard$ = () => (
    <Provider store={store}>
      <MyPokemonCard pokemon={pokemonObj} />
    </Provider>
  );

  it('Match Snapshot!', () => {
    const component = renderer.create(MyPokemonCard$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should not display nickname', () => {
    const wrapper = mount(MyPokemonCard$());
    expect(wrapper.children().find('.nickname-button')).toHaveLength(1);
  });

  it('Should display nickname', () => {
    const pokemonObjWithNickname = {
      ...pokemonObj,
      nickname: 'test',
    };

    const MyPokemonCardConnected = () => (
      <Provider store={store}>
        <MyPokemonCard pokemon={pokemonObjWithNickname} />
      </Provider>
    );

    const wrapper = mount(MyPokemonCardConnected());
    expect(wrapper.children().find('.mypokemon-nickname').text()).toEqual('test');
  });

  it('Should dispatch release actions', () => {
    const wrapper = mount(MyPokemonCard$());
    wrapper.children().find('.secondary-button').props().onClick();

    const actions = store.getActions();
    const expected = {
      type: '[My Pokemon] Release',
      payload: {
        pokemonId: 'pokemon-01',
      },
    };
    expect(actions[0]).toEqual(expected);
  });
});
