// PokedexStore Test
import { pokedexReducer } from '../store/Pokedex';

const initialState = {
  pokemons: [],
  pokemonsIsFetching: false,
  pokemonsError: {},
  count: 0,
};

describe('PokedexStore', () => {
  describe('Reducer', () => {
    it('Should return correct state on PokedexRequest', () => {
      const action = { type: '[POKEDEX] Fetch Request' };
      const result = pokedexReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonsIsFetching: true,
        pokemonsError: {},
        count: 0,
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on PokedexSuccess', () => {
      const action = {
        type: '[POKEDEX] Fetch Success',
        payload: {
          data: {
            results: [
              {
                name: 'bulbasaur',
              },
              {
                name: 'ivysaur',
              },
              {
                name: 'venusaur',
              },
            ],
            count: 3,
          },
        },
      };
      const result = pokedexReducer(initialState, action);
      const expected = {
        pokemons: [
          {
            name: 'bulbasaur',
          },
          {
            name: 'ivysaur',
          },
          {
            name: 'venusaur',
          },
        ],
        pokemonsIsFetching: false,
        pokemonsError: {},
        count: 3,
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return current state on default', () => {
      const action = { type: '[NOT DEFINED ACTION]' };
      const result = pokedexReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonsIsFetching: false,
        pokemonsError: {},
        count: 0,
      };

      expect(result).toStrictEqual(expected);
    });
  });
});
