// PokemonStore Test
import {
  pokemonRequest, pokemonSuccess, pokemonFailed, pokemonAdd, pokemonUpdate, pokemonReducer,
} from '../store/Pokemon';

const initialState = {
  pokemons: [],
  pokemonIsFetching: false,
  pokemonError: {},
};

describe('PokemonStore', () => {
  describe('Actions Creator', () => {
    it('Should dispatch PokemonRequest', () => {
      const result = pokemonRequest();
      const expected = {
        type: '[POKEMON] Fetch Request',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should dispatch PokemonSuccess', () => {
      const result = pokemonSuccess();
      const expected = {
        type: '[POKEMON] Fetch Success',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should dispatch PokemonFailed', () => {
      const failedMessage = 'Failed to fetch!';
      const result = pokemonFailed(failedMessage);
      const expected = {
        type: '[POKEMON] Fetch Failed',
        payload: 'Failed to fetch!',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should dispatch PokemonAdd', () => {
      const pokemonData = {
        id: 1,
        name: 'bulbasaur',
        height: 30,
        weight: 60,
        sprites: {},
        stats: [],
        types: [],
      };
      const result = pokemonAdd(pokemonData);
      const expected = {
        type: '[POKEMON] Add',
        payload: {
          data: {
            id: 1,
            name: 'bulbasaur',
            height: 30,
            weight: 60,
            sprites: {},
            stats: [],
            types: [],
          },
        },
      };
      expect(result).toStrictEqual(expected);
    });

    it('Should dispatch PokemonUpdate', () => {
      const pokemonData = {
        id: 1,
        capture_rate: 30,
        base_happiness: 60,
        flavor_text_entries: [
          {
            flavor_text: 'For some time after its birth, it grows by gaining nourishment from the seed on its back.',
            language: { name: 'en' },
            version: { name: 'y' },
          },
        ],
        genera: [
          {
            genus: 'Seed Pokémon',
            language: { name: 'en' },
          },
        ],
      };
      const result = pokemonUpdate(pokemonData);
      const expected = {
        type: '[POKEMON] Update',
        payload: {
          data: {
            id: 1,
            baseHappiness: 60,
            captureRate: 30,
            classification: 'Seed Pokémon',
            flavorText: 'For some time after its birth, it grows by gaining nourishment from the seed on its back.',
          },
        },
      };
      expect(result).toStrictEqual(expected);
    });
  });

  describe('Reducer', () => {
    it('Should return correct state on PokemonRequest', () => {
      const action = { type: '[POKEMON] Fetch Request' };
      const result = pokemonReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonIsFetching: true,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on PokemonSuccess', () => {
      const action = { type: '[POKEMON] Fetch Success' };
      const result = pokemonReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on PokemonFailed', () => {
      const action = {
        type: '[POKEMON] Fetch Failed',
        payload: 'Failed to fetch',
      };
      const result = pokemonReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on PokemonAdd', () => {
      const action = {
        type: '[POKEMON] Add',
        payload: {
          data: {
            id: 1,
            name: 'bulbasaur',
            sprites: {},
            stats: [],
            types: [],
          },
        },
      };
      const result = pokemonReducer(initialState, action);
      const expected = {
        pokemons: [
          {
            id: 1,
            name: 'bulbasaur',
            sprites: {},
            stats: [],
            types: [],
          },
        ],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on PokemonUpdate', () => {
      const action = {
        type: '[POKEMON] Update',
        payload: {
          data: {
            id: 1,
            name: 'bulbasaur',
            captureRate: 50,
            baseHappiness: 100,
            flavorText: 'For some time after its birth, it grows by gaining nourishment from the seed on its back.',
            classification: 'Seed Pokémon',
          },
        },
      };

      const initialStateUpdate = {
        pokemons: [
          {
            id: 1,
            name: 'bulbasaur',
            sprites: {},
            stats: [],
            types: [],
          },
        ],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      const result = pokemonReducer(initialStateUpdate, action);
      const expected = {
        pokemons: [
          {
            id: 1,
            name: 'bulbasaur',
            sprites: {},
            stats: [],
            types: [],
            captureRate: 50,
            baseHappiness: 100,
            flavorText: 'For some time after its birth, it grows by gaining nourishment from the seed on its back.',
            classification: 'Seed Pokémon',
          },
        ],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return current state on default', () => {
      const action = { type: '[NOT DEFINED ACTION]' };
      const result = pokemonReducer(initialState, action);
      const expected = {
        pokemons: [],
        pokemonIsFetching: false,
        pokemonError: {},
      };

      expect(result).toStrictEqual(expected);
    });
  });
});
