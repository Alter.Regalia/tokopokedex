// UtilsStore Test
import {
  utilsPokedexSelect, utilsMyPokemonSelect, utilsReset, utilsReducer,
} from '../store/Utils';

const InitialState = {
  selectedName: '',
  selectedMyPokemon: '',
};

describe('UtilsStore', () => {
  describe('Actions Creator', () => {
    it('Should dispatch UtilsPokedexSelect', () => {
      const result = utilsPokedexSelect('bulbasaur');
      const expected = {
        type: '[Utils] Select Pokedex',
        payload: {
          name: 'bulbasaur',
        },
      };

      expect(result).toMatchObject(expected);
    });

    it('Should dispatch UtilsMyPokemonSelect', () => {
      const result = utilsMyPokemonSelect('bulbasaur');
      const expected = {
        type: '[Utils] Select MyPokemon',
        payload: {
          id: 'bulbasaur',
        },
      };

      expect(result).toMatchObject(expected);
    });

    it('Should dispatch UtilsReset', () => {
      const result = utilsReset();
      const expected = {
        type: '[Utils] Reset',
      };

      expect(result).toMatchObject(expected);
    });
  });

  describe('Reducer', () => {
    it('Should return correct state on PokedexSelect', () => {
      const action = utilsPokedexSelect('bulbasaur');
      const result = utilsReducer(InitialState, action);

      const expected = {
        selectedName: 'bulbasaur',
        selectedMyPokemon: '',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on MyPokemonSelect', () => {
      const action = utilsMyPokemonSelect('bulbasaur');
      const result = utilsReducer(InitialState, action);

      const expected = {
        selectedName: '',
        selectedMyPokemon: 'bulbasaur',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return correct state on Reset', () => {
      const action = utilsReset();
      const result = utilsReducer(InitialState, action);

      const expected = {
        selectedName: '',
        selectedMyPokemon: '',
      };

      expect(result).toStrictEqual(expected);
    });

    it('Should return current state on default', () => {
      const action = { type: '[NOT DEFINED ACTION]' };
      const result = utilsReducer(InitialState, action);

      const expected = {
        selectedName: '',
        selectedMyPokemon: '',
      };

      expect(result).toStrictEqual(expected);
    });
  });
});
