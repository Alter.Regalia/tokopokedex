// MyPokemonStore Test
import {
  myPokemonAdd, myPokemonRelease, myPokemonUpdate, myPokemonReset, myPokemonReducer,
} from '../store/MyPokemon';

const pokemonObj = {
  pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
  catchAt: '2019-12-12T07:40:15.286Z',
  id: 2,
  name: 'ivysaur',
  nickname: '',
};

describe('MyPokemonStore', () => {
  describe('Actions Creator', () => {
    it('Should dispatch MyPokemonAdd', () => {
      const result = myPokemonAdd(pokemonObj);
      const expected = {
        type: '[My Pokemon] Add',
        payload: {
          pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
          catchAt: '2019-12-12T07:40:15.286Z',
        },
      };

      expect(result).toMatchObject(expected);
    });

    it('Should dispatch MyPokemonRelease', () => {
      const result = myPokemonRelease(pokemonObj.pokemonId);
      const expected = {
        type: '[My Pokemon] Release',
        payload: {
          pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
        },
      };

      expect(result).toMatchObject(expected);
    });

    it('Should dispatch MyPokemonUpdate', () => {
      const result = myPokemonUpdate(pokemonObj);
      const expected = {
        type: '[My Pokemon] Update',
        payload: {
          pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
          catchAt: '2019-12-12T07:40:15.286Z',
          id: 2,
          name: 'ivysaur',
          nickname: '',
        },
      };

      expect(result).toMatchObject(expected);
    });

    it('Should dispatch MyPokemonReset', () => {
      const result = myPokemonReset();
      const expected = {
        type: '[My Pokemon] Reset',
      };

      expect(result).toMatchObject(expected);
    });
  });

  describe('Reducer', () => {
    it('Should return correct state on Add', () => {
      const InitialState = {
        pokemons: [],
      };

      const expected = {
        pokemons: [
          {
            catchAt: '2019-12-12T07:40:15.286Z',
            id: 2,
            name: 'ivysaur',
            nickname: '',
            pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
          },
        ],
      };

      const action = myPokemonAdd(pokemonObj);
      const result = myPokemonReducer(InitialState, action);

      expect(result).toStrictEqual(expected);
    });
  });

  it('Should return correct state on Release', () => {
    const InitialState = {
      pokemons: [{
        catchAt: '2019-12-12T07:40:15.286Z',
        id: 2,
        name: 'ivysaur',
        nickname: '',
        pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
      }],
    };

    const expected = {
      pokemons: [],
    };

    const action = myPokemonRelease('d07dcc1e-7265-48cd-b4ab-7f732ee852e2');
    const result = myPokemonReducer(InitialState, action);

    expect(result).toStrictEqual(expected);
  });

  it('Should return correct state on Update', () => {
    const InitialState = {
      pokemons: [{
        catchAt: '2019-12-12T07:40:15.286Z',
        id: 2,
        name: 'ivysaur',
        nickname: '',
        pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
      }],
    };

    const pokemonData = {
      catchAt: '2019-12-12T07:40:15.286Z',
      id: 2,
      name: 'ivysaur',
      nickname: 'Nickname',
      pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
    };

    const expected = {
      pokemons: [
        {
          catchAt: '2019-12-12T07:40:15.286Z',
          id: 2,
          name: 'ivysaur',
          nickname: 'Nickname',
          pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
        },
      ],
    };

    const action = myPokemonUpdate(pokemonData);
    const result = myPokemonReducer(InitialState, action);

    expect(result).toStrictEqual(expected);
  });

  it('Should return correct state on Reset', () => {
    const InitialState = {
      pokemons: [{
        catchAt: '2019-12-12T07:40:15.286Z',
        id: 2,
        name: 'ivysaur',
        nickname: '',
        pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
      }],
    };

    const expected = {
      pokemons: [],
    };

    const action = myPokemonReset();
    const result = myPokemonReducer(InitialState, action);

    expect(result).toStrictEqual(expected);
  });

  it('Should return current state on default', () => {
    const InitialState = {
      pokemons: [{
        catchAt: '2019-12-12T07:40:15.286Z',
        id: 2,
        name: 'ivysaur',
        nickname: '',
        pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
      }],
    };

    const expected = {
      pokemons: [{
        catchAt: '2019-12-12T07:40:15.286Z',
        id: 2,
        name: 'ivysaur',
        nickname: '',
        pokemonId: 'd07dcc1e-7265-48cd-b4ab-7f732ee852e2',
      }],
    };

    const action = { type: '[NOT DEFINED ACTION]' };
    const result = myPokemonReducer(InitialState, action);

    expect(result).toStrictEqual(expected);
  });
});
