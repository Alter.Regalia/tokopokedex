import React from 'react';
import renderer from 'react-test-renderer';
import { shallow, mount } from 'enzyme';

import Modal from '../components/Modal';

describe('Modal', () => {
  it('Match Snapshot!', () => {
    const component = renderer.create(<Modal />);
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should show', () => {
    const wrapper = shallow(<Modal />);
    wrapper.setProps({ show: true, close: jest.fn() });

    const modal = wrapper.find('.modal').hasClass('modal-active');
    expect(modal).toBeTruthy();
  });

  it('Should not show', () => {
    const wrapper = shallow(<Modal />);
    wrapper.setProps({ show: false, close: jest.fn() });

    const modal = wrapper.find('.modal').hasClass('modal-active');
    expect(modal).toBeFalsy();
  });

  it('Should close on exit', () => {
    const wrapper = mount(<Modal />);
    wrapper.setProps({ show: false });
    wrapper.simulate('keydown', { keyCode: 27 });

    expect(wrapper.prop('show')).toEqual(false);
  });
});
