// Utils Store Selector Test
import { UtilsStoreSelector, selectedNameSelector, selectedMyPokemonSelector } from '../store/Utils.selector';

const globalStore = {
  PokedexStore: {},
  MyPokemonStore: {},
  UtilsStore: {
    selectedName: 'bulbasaur',
    selectedMyPokemon: 'ivysaur',
  },
};

describe('UtilsStore selector', () => {
  it('Should return UtilStore', () => {
    const result = UtilsStoreSelector(globalStore);
    const expected = {
      selectedName: 'bulbasaur',
      selectedMyPokemon: 'ivysaur',
    };

    expect(result).toStrictEqual(expected);
  });

  it('Should return SelectedName', () => {
    const result = selectedNameSelector(globalStore);
    const expected = 'bulbasaur';

    expect(result).toStrictEqual(expected);
  });

  it('Should return SelectedMyPokemon', () => {
    const result = selectedMyPokemonSelector(globalStore);
    const expected = 'ivysaur';

    expect(result).toStrictEqual(expected);
  });
});
