// PokedexDetail Test
import React from 'react';
import thunk from 'redux-thunk';
import renderer from 'react-test-renderer';
import configureStore from 'redux-mock-store';
import { Provider } from 'react-redux';

import PokedexDetail from '../components/PokedexDetail';

// Mock Store
const mockStore = configureStore([thunk]);
const initialState = {
  PokemonStore: {
    pokemons: [
      {
        id: 1,
        name: 'bulbasaur',
        sprites: {},
        stats: [],
        types: [],
        height: 30,
        weight: 15,
      },
    ],
  },
  UtilsStore: {
    selectedName: 'bulbasaur',
  },
};
const store = mockStore(initialState);

describe('PokedexDetail', () => {
  const PokedexDetail$ = () => (
    <Provider store={store}>
      <PokedexDetail />
    </Provider>
  );

  it('Match Snapshot!', () => {
    const component = renderer.create(PokedexDetail$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });
});
