import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

import PokedexCard from '../components/PokedexCard';

describe('PokedexCard', () => {
  const pokemonObj = {
    name: 'bulbasaur',
  };

  const PokedexCard$ = () => (<PokedexCard pokemon={pokemonObj} />);

  it('Match Snapshot!', () => {
    const component = renderer.create(PokedexCard$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should render pokemon name', () => {
    const wrapper = mount(<PokedexCard pokemon={pokemonObj} />);
    expect(wrapper.children().find('.pokedex-card').text()).toBe('bulbasaur');
  });

  it('Should render pokemon image', () => {
    const wrapper = mount(<PokedexCard pokemon={pokemonObj} index={1} />);
    expect(wrapper.children().find('img').prop('src')).toEqual('https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png');
  });

  it('Should have alt of pokemon name', () => {
    const wrapper = mount(<PokedexCard pokemon={pokemonObj} index={1} />);
    expect(wrapper.children().find('img').prop('alt')).toEqual('bulbasaur');
  });
});
