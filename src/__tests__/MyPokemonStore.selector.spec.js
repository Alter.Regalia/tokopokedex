// MyPokemon Store Selector Test
import {
  myPokemonStoreSelector, pokemonsSelector, pokemonByIdSelector, pokemonKindSelector,
} from '../store/MyPokemon.selector';

const globalStore = {
  PokedexStore: {},
  MyPokemonStore: {
    pokemons: [
      {
        name: 'bulbasaur',
        pokemonId: 'pokemon-01',
      },
      {
        name: 'ivysaur',
        pokemonId: 'pokemon-02',
      },
      {
        name: 'venusaur',
        pokemonId: 'pokemon-03',
      },
    ],
  },
  UtilsStore: {
    selectedName: '',
    selectedMyPokemon: 'pokemon-01',
  },
};

describe('MyPokemonStore selector', () => {
  it('Should return MyPokemonStore', () => {
    const result = myPokemonStoreSelector(globalStore);
    const expected = {
      pokemons: [
        {
          name: 'bulbasaur',
          pokemonId: 'pokemon-01',
        },
        {
          name: 'ivysaur',
          pokemonId: 'pokemon-02',
        },
        {
          name: 'venusaur',
          pokemonId: 'pokemon-03',
        },
      ],
    };

    expect(result).toStrictEqual(expected);
  });

  it('Should return Pokemons', () => {
    const result = pokemonsSelector(globalStore);
    const expected = [
      {
        name: 'bulbasaur',
        pokemonId: 'pokemon-01',
      },
      {
        name: 'ivysaur',
        pokemonId: 'pokemon-02',
      },
      {
        name: 'venusaur',
        pokemonId: 'pokemon-03',
      },
    ];

    expect(result).toStrictEqual(expected);
  });

  it('Should return PokemonsById', () => {
    const result = pokemonByIdSelector(globalStore);
    const expected = {
      name: 'bulbasaur',
      pokemonId: 'pokemon-01',
    };

    expect(result).toStrictEqual(expected);
  });

  it('Should return PokemonKind', () => {
    const result = pokemonKindSelector(globalStore);
    const expected = 3;

    expect(result).toStrictEqual(expected);
  });
});
