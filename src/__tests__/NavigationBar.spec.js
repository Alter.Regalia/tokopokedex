import React from 'react';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';
import { MemoryRouter } from 'react-router-dom';

import NavigationBar from '../components/NavigationBar';

describe('NavigationBar', () => {
  const NavigationBar$ = () => (
    <MemoryRouter>
      <NavigationBar />
    </MemoryRouter>
  );

  it('Match Snapshot!', () => {
    const component = renderer.create(NavigationBar$());
    const tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it('Should render', () => {
    const wrapper = mount(NavigationBar$());
    const navigationBar = wrapper.find('.navigation-bar');
    expect(navigationBar).toHaveLength(1);
  });
});
