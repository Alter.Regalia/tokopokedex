// Pokedex
import React, { useState, useContext, useEffect } from 'react';
import { connect } from 'react-redux';

import Modal from '../components/Modal';
import PokedexCard from '../components/PokedexCard';
import PokedexDetail from '../components/PokedexDetail';
import InfiniteScroll from '../components/InfiniteScroll';

import { fetchPokedex } from '../store/Pokedex';
import { fetchPokemon } from '../store/Pokemon';
import { utilsPokedexSelect } from '../store/Utils';

const Pokedex = ({
  pokedex, fetch, getPokemon, updateSelected,
}) => {
  const fetchMore = useContext(InfiniteScroll);
  const { pokemons, pokemonsIsFetching, count } = pokedex;

  const [limit, setLimit] = useState(30);
  const [offset, setOffset] = useState(0);
  const [modalOpen, setModalOpen] = useState(false);

  const showModal = () => {
    setModalOpen(true);
  };

  const hideModal = () => {
    setModalOpen(false);
  };

  const selectPokemon = (pokemon) => {
    updateSelected(pokemon.name);
    getPokemon(pokemon.name);
    showModal();
  };

  const fetchMorePokemon = () => {
    setOffset(offset + 30);
  };


  useEffect(() => {
    if (fetchMore) {
      fetchMorePokemon();
    }
  }, [fetchMore]);

  useEffect(() => {
    if (pokemons.length > offset) {
      setOffset(pokemons.length);
    } else {
      fetch(limit, offset);
    }
  }, [offset]);

  const handleOffsetSelection = (event) => {
    event.preventDefault();
    setLimit(event.target.value);
  };

  return (
    <>
      <div className="header-bar">
        <p>
          {`${pokemons.length} / ${count} Pokemon`}
        </p>
        {pokemonsIsFetching && '...'}
        <select className="limit-select" onChange={(event) => handleOffsetSelection(event)}>
          {[30, 60, 90, 120].map((itemLimit) => <option key={itemLimit}>{itemLimit}</option>)}
        </select>
      </div>
      <div className="pokedex-container">
        {pokemons.length !== 0
          && pokemons.map(
            (pokemon, index) => (
              <PokedexCard
                index={index + 1}
                key={pokemon.name}
                pokemon={pokemon}
                onCardClick={() => selectPokemon(pokemon)}
              />
            ),
          )}
      </div>
      <Modal show={modalOpen} close={hideModal}>
        <PokedexDetail />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  pokedex: state.PokedexStore,
});

const mapDispatchToProps = (dispatch) => ({
  fetch: (limit, offset) => {
    dispatch(fetchPokedex(limit, offset));
  },
  getPokemon: (url) => {
    dispatch(fetchPokemon(url));
  },
  updateSelected: (name) => {
    dispatch(utilsPokedexSelect(name));
  },
});


export default connect(mapStateToProps, mapDispatchToProps)(Pokedex);
