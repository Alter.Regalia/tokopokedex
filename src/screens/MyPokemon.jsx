// MyPokemon
import React, { useState } from 'react';
import { connect } from 'react-redux';

import MyPokemonCard from '../components/MyPokemonCard';
import Modal from '../components/Modal';
import MyPokemonDetail from '../components/MyPokemonDetail';

import { pokemonsSelector, pokemonKindSelector } from '../store/MyPokemon.selector';
import { myPokemonRelease } from '../store/MyPokemon';
import { utilsMyPokemonSelect } from '../store/Utils';

const MyPokemon = ({ myPokemons, updateSelected, kind }) => {
  const [modalOpen, setModalOpen] = useState(false);

  const showModal = () => {
    setModalOpen(true);
  };

  const hideModal = () => {
    setModalOpen(false);
  };

  const selectPokemon = (pokemonId) => {
    updateSelected(pokemonId);
    showModal();
  };

  return (
    <>
      <div className="header-bar">
        <p>MyPokemon</p>
        {kind !== 0 && <p>{`${kind} Species Caught`}</p>}
      </div>
      <div className="mypokemon-container">
        {myPokemons.length === 0
          ? (<p>You have not caught any pokemon :(</p>)
          : (myPokemons.map((pokemon) => (
            <MyPokemonCard
              key={pokemon.pokemonId}
              pokemon={pokemon}
              selectPokemon={() => selectPokemon(pokemon.pokemonId)}
            />
          )))}
      </div>
      <Modal show={modalOpen} close={hideModal}>
        <MyPokemonDetail afterUpdate={hideModal} />
      </Modal>
    </>
  );
};

const mapStateToProps = (state) => ({
  myPokemons: pokemonsSelector(state),
  kind: pokemonKindSelector(state),
});

const mapDispatchToProps = (dispatch) => ({
  updateSelected: (pokemonId) => {
    dispatch(utilsMyPokemonSelect(pokemonId));
  },
  releasePokemon: (pokemonId) => {
    dispatch(myPokemonRelease(pokemonId));
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MyPokemon);
