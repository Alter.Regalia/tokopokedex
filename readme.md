# TokoPokedex

<p align="center">
  <img src="assets/tokopokedex.png" alt="tokopokedex">
</p>

## Live URL

- [TokoPokedex on surge.sh](https://toko-pokedex.surge.sh/)
- [TokoPokedex on gitlab-page](https://alter.regalia.gitlab.io/tokopokedex/)

_( these apps are running on shared space, might takes minutes or so before they wake up and accessible )_

## Task

- Consume [PokeAPI](https://pokeapi.co/)
- Build Pokedex with Pokemon List, Pokemon Detail, and My Pokemon page
- Each Pokemon can be caught with probability of 50%
- Give caught pokemon nickname

## Tech Stacks

- [React](https://reactjs.org)
- [Redux](https://redux.js.org/)
- [SASS/SCSS](https://sass-lang.com)
- [Jest](https://jestjs.io/)
- [Parcel](https://parceljs.org)
- [Babel](https://babeljs.io/)
- [ESLint](https://eslint.org/)

## How To Run Locally

_prerequisites_

- [Git](https://git-scm.com/)
- [Node.js](https://nodejs.org/en/)
- [Yarn](https://yarnpkg.com/en/)

1. Clone TokoPokedex repository

```
$ git clone https://gitlab.com/Alter.Regalia/tokopokedex.git
```

2. Move into **tokopokedex** directory

```
$ cd tokopokedex
```

3. Install the required module packages

```
$ yarn
```

4. Run the applications

```
$ yarn start
```

The browser windows will automatically open the applications on http://localhost:1234

_( If is not, check if the port is being used or not, or just open the url on your browser )_

## Production Build

1. To build distribution package, set _NODE_ENV_ to **production**

```
$ set NODE_ENV=production
```

2. then simply run **yarn build** command

```
$ yarn build
```

3. Distribution package are available in **dist** folder

```
___dist
|___index.html
|___src.*******.js
|___src.*******.js.map
|___src.*******.css
|___src.*******.css.map

```

4. Deploy it somewhere~

## Code Coverage

[TokoPokedex Code Coverage](https://tokopokedex-coverage.surge.sh/) - Generate applications code coverage

```
$ yarn test:coverage
```

---

📧 [Ariiq Dio Fathurrachman](mailto:ariiq.dio.f@gmail.com)
